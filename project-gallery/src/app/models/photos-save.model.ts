export class SavePhotos{
    title:string;
    url:string;
    description:string;
    constructor(t:string,u:string,d:string){
        this.title = t;
        this.url = u;
        this.description = d;
    }
}