import { Component, OnInit } from '@angular/core';
import { SavePhotos} from '../models/photos-save.model';

@Component({
  selector: 'app-list-photos',
  templateUrl: './list-photos.component.html',
  styleUrls: ['./list-photos.component.css']
})
export class ListPhotosComponent implements OnInit {
  photos: SavePhotos[];
  constructor() { 
    this.photos = [];
  }

  ngOnInit(): void {
  }
  guardar(title:string, url:string, description:string):boolean{
    if(title != "" && url != "" && description != ""){
      this.photos.push(new SavePhotos(title,url,description));
    }
    return false;
  }
}
