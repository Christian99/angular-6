export class DestinoViajes{
    private selected:boolean;
    public servicio: String[];

    constructor(public nombre:string,public imagenUrl:string){
        this.servicio= ['Descargar','Publicar'];
    }

    isSelected():boolean {
        return this.selected;
    }

    setSelected(s: boolean){
        this.selected = s;
    }
}