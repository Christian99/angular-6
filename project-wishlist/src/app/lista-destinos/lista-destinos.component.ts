import { Component, OnInit } from '@angular/core';
import { DestinoViajes} from '../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViajes[];
  constructor() { 
    this.destinos = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean {
    this.destinos.push(new DestinoViajes(nombre,url));
    console.log(this.destinos);
    return false;
  }

  elegido(d: DestinoViajes){
    this.destinos.forEach(function(x){
      x.setSelected(false);
    });
    d.setSelected(true);
  }

}
