import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { SavePhotos} from '../models/photos-save.model';

@Component({
  selector: 'app-gallery-photos',
  templateUrl: './gallery-photos.component.html',
  styleUrls: ['./gallery-photos.component.css']
})
export class GalleryPhotosComponent implements OnInit {
  @Input() photos: SavePhotos;
  @HostBinding('attr.class') cssClass = "col-md-4 my-3"
  constructor() { }

  ngOnInit(): void {
  }

}
