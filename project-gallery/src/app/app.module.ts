import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListPhotosComponent } from './list-photos/list-photos.component';
import { GalleryPhotosComponent } from './gallery-photos/gallery-photos.component';

@NgModule({
  declarations: [
    AppComponent,
    ListPhotosComponent,
    GalleryPhotosComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
