import { Component, OnInit,Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViajes } from '../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})

export class DestinoViajeComponent implements OnInit {
  @Input() destino:DestinoViajes;
  @HostBinding('attr.class') cssClass= "col-md-4 my-3";
  @Output() clicked: EventEmitter<DestinoViajes>
  
  constructor() { 
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destino);
    return false;
  }

}
